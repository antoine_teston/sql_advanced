-----------------------------------------------
-- TD SQL_advanced
-----------------------------------------------

-----------------------------------------------
-- Objectifs
-----------------------------------------------
-- Octobre 2022, M1 ILIS_DSS
-- Créations de script permettant la création des tables
-- qui servira d'import de csv
-- création des clés primaire, étrangères
-----------------------------------------------

-----------------------------------------------
-- SESSION INFO
-----------------------------------------------
-- Dbeaver Version 22.2.2.202210092258
-- VSCode Version : 1.72.0
-- macOS Monterey/WIN 11
-- git https://gitlab.com/antoine_teston/sql_advanced
-----------------------------------------------

-----------------------------------------------
-----------------------------------------------

-- Création des tables

-- Importation des confirmed_status_date/datetime des tables person et visit en VARCHAR 
-- suite à erreurs de date ou de format au moment de l'import. Formatage des formats
-- à la suite du script


CREATE TABLE if NOT exists person (
     PERSON_ID                INT PRIMARY KEY     NOT NULL,
     PERSON_NAME              VARCHAR(20) 	      NOT NULL,
     HEALTH_STATUS            VARCHAR(15) 	      NOT NULL,
     CONFIRMED_STATUS_DATE    VARCHAR(50) 		  NOT NULL 
)

CREATE TABLE if NOT exists place (
     PLACE_ID                INT PRIMARY KEY    NOT NULL,
     PLACE_NAME              VARCHAR(50) 	    NOT NULL,
     PLACE_TYPE              VARCHAR(30)        NOT NULL
)

CREATE TABLE if NOT exists visit (
    VISIT_ID                INT PRIMARY KEY   NOT NULL,
    PERSON_ID               INT               NOT NULL references person(PERSON_ID),
    PLACE_ID                INT               NOT NULL references place(PLACE_ID),
    START_DATETIME          VARCHAR(50) 	  NOT NULL,
    END_DATETIME            VARCHAR(50) 	  NOT NULL
)

-----------------------------------------------
-- Suppression des tables

DROP TABLE if exists visit

DROP TABLE if exists place

DROP TABLE if exists person

-----------------------------------------------

-- Insertions des données

-- COPY person(person_id, person_name, health_status, confirmed_status_date)
-- FROM "./data/person.csv" 
-- DELIMITER ";" 
-- CSV HEADER

-- COPY place(place_id, place_name, place_type
-- FROM "./data/place.csv" 
-- DELIMITER ";" 
-- CSV HEADER

-- COPY visit(visit_id, person_id, place_id, start_datetime, end_datetime)
-- FROM "./data/visit.csv" 
-- DELIMITER ";" 
-- CSV HEADER

-- Non applicable car serveur distant, pas de droit d'accès

-----------------------------------------------

-- Modification en TIMESTAMP par création/duplication/renommage/suppresion colonne d'origine

-- person p
ALTER TABLE person ADD COLUMN new_ts TIMESTAMP ;
UPDATE person SET new_ts  = to_timestamp(confirmed_status_date , 'dd/MM/YYYY HH24:MI') ;

ALTER TABLE person DROP COLUMN confirmed_status_date;
ALTER TABLE person rename column new_ts to confirmed_status_date; 

-- visit v
ALTER TABLE visit ADD COLUMN new_ts1 TIMESTAMP, ADD COLUMN new_ts2 TIMESTAMP ;
UPDATE visit SET new_ts1  = to_timestamp(start_datetime , 'dd/MM/YYYY HH24:MI');
UPDATE visit SET new_ts2 = to_timestamp(end_datetime, 'dd/MM/YYYY HH24:MI');

ALTER TABLE visit DROP COLUMN start_datetime, DROP COLUMN end_datetime;
ALTER TABLE visit rename column new_ts1 to start_datetime; 
ALTER TABLE visit rename column new_ts2 to end_datetime; 

-----------------------------------------------

