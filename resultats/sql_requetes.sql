-----------------------------------------------
-- TD SQL_advanced
-----------------------------------------------

-----------------------------------------------
-- Objectifs
-----------------------------------------------
-- Octobre 2022, M1 ILIS_DSS
-- Noms + statuts santé des personnes que Landyn Greer a croisé (même lieu, même moment)
-- Nombre de malades croisés dans un bar (même moment)
-- Noms des personnes que Taylor Luna a croisé
-- Nombre de malades par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
-- Nombre de sains (non malades) par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
-----------------------------------------------

-----------------------------------------------
-- SESSION INFO
-----------------------------------------------
-- Dbeaver Version 22.2.2.202210092258
-- VSCode Version : 1.72.0
-- macOS Monterey/WIN 11
-- git https://gitlab.com/antoine_teston/sql_advanced
-----------------------------------------------

-----------------------------------------------
-----------------------------------------------

-- 1. Noms/Statuts personnes ayant croisé Landyn Greer

-- 1.1. Affichage des lieux qu'à fréquenté Landyn
select v.place_id, pl.place_name
from visit v inner join place pl
on v.place_id = pl.place_id 
where person_id = 1;

-- 1.2.1 Affichage des noms/statuts ayant fréquenté le lieu 33
select p.person_name, p.health_status
from person p inner join visit v 
on p.person_id = v.person_id
where place_id = 33 
order by p.person_id 

-- 1.2.2 Affichage des noms/statuts ayant fréquenté le lieu 88
select p.person_name, p.health_status
from person p inner join visit v 
on p.person_id = v.person_id
where place_id = 88 
order by p.person_id 

-- 1.3.1 Affichage des noms/status que L.G a fréquenté au moment de son passage
-- sur le lieu 33

select p.person_name, p.health_status
from person p inner join visit v 
on p.person_id = v.person_id
where tsrange(start_datetime, end_datetime) &&
	  tsrange '(2020-10-07 20:54:00.000,2020-10-08 03:44:00.000)'
and p.person_name != 'Landyn Greer'
and v.place_id = 33

-- 1.3.2 Affichage des noms/status que L.G a fréquenté au moment de son passage
-- sur le lieu 88

select p.person_name, p.health_status 
from person p inner join visit v 
on p.person_id = v.person_id
where tsrange(start_datetime, end_datetime) &&
	  tsrange '(2020-10-07 20:54:00.000,2020-10-08 03:44:00.000)'
and p.person_name != 'Landyn Greer'
and v.place_id = 88


-- On regroupe l'ensemble des noms/statuts que LG a fréquenté : 
select p.person_name as "LG_crossed_people", p.health_status as "status"
from person p inner join visit v 
on p.person_id = v.person_id
where tsrange(start_datetime, end_datetime) &&
	  tsrange '(2020-10-07 20:54:00.000,2020-10-08 03:44:00.000)'
and p.person_name != 'Landyn Greer'
and v.place_id IN (33,88)
-- EXPORT

-- 2. Compter le nombre de malades croisés dans un bar (même moment)
-- 2.1 Création d'une nouvelle table avec jointure des 3 tables visit, person, place
create table jointab
	(VISIT_ID				 INT      				 null,
	 PERSON_ID               INT      				 NULL,
	 PLACE_ID                INT     				 NULL,
	 START_DATETIME          TIMESTAMP 	 	 		 NULL,
     END_DATETIME            TIMESTAMP 	 			 null,
     PERSON_NAME             VARCHAR(20) 	      	 NULL,
     HEALTH_STATUS           VARCHAR(10) 	      	 NULL,
     CONFIRMED_STATUS_DATE   TIMESTAMP 		  		 null,
     PLACE_NAME				 VARCHAR(30)			 null,
     PLACE_TYPE              VARCHAR(30)        	 null
     );

-- 2.1.bis Suppression de table si besoin 
    
drop table if exists jointab

-- 2.2 Insertion des données nouvelle table

insert into jointab 
	select v.VISIT_ID, v.PERSON_ID, v.PLACE_ID, v.START_DATETIME, v.END_DATETIME,
	p.PERSON_NAME, p.HEALTH_STATUS , p.CONFIRMED_STATUS_DATE, pl.place_name, pl.PLACE_TYPE 
	from visit v
	inner join person p on v.person_id = p.person_id
	inner join place pl on v.place_id  = pl.place_id;
	 

-- 2.3 Filtre des visites pour les gens malades avec le type de Place 
-- ainsi qu'une date de déclaration malade inférieure/égale à la date de visite (pas certain de la démarche)

-- Création table temporaire pour effectuer une jointure de table
create temp table sick_bar as 
	select * from jointab
	where health_status = 'Sick'
	and place_type = 'Bar'
	and confirmed_status_date <= start_datetime 

-- 2.3.1 Nombre de personnes malades distinct ayant fréquenté chaque bar toute période confondue
select place_name as "Bar", count(person_id) as "Nb de personnes"
from sick_bar 
group by place_name 

-- 2.3.2 Nombre de personnes malades distinct ayant fréquenté chaque bar en même temps 
select t1.place_name as "bars_name", count(distinct t2.person_id) as "sick_people"
from sick_bar t1, sick_bar t2
where t2.place_id = t1.place_id
and t1.start_datetime <= t2.end_datetime
and t1.end_datetime >= t2.start_datetime
and t1.person_name != t2.person_name 
group by t1.place_name
-- EXPORT
     
-- 3. Noms des personnes que Taylor Luna a croisé
-- 3.1 Création temporaire d'une nouvelle table
create temp table tl_tracing as
	select * from jointab
	where person_name = 'Taylor Luna'

-- 3.2 Affichage des visites de Taylor Luna indépendamment de son statut
select *
from tl_tracing
	
-- 3.3 Affichage des personnes que Taylor Luna a croisé au moment de ses visites
select distinct t1.person_name as "TL_crossed_ppl"
from jointab t1 inner join tl_tracing t2
on t1.place_id = t2.place_id
where t1.start_datetime <= t2.end_datetime
and t1.end_datetime >= t2.start_datetime
and t1.person_name != t2.person_name
-- EXPORT

-- 4. Nombre de malades par endroit (place_name)
-- + durée moyenne de leurs visites dans cet endroit

-- 4.1 Création d'un intervalle de temps de visite par ajout sur jointab
alter table jointab add column duree interval;
update jointab set duree = age(end_datetime, start_datetime) -- Calcul d'intervalle

-- 4.2 Nombre de malades groupé par endroit
select place_name as "place", count(person_id) as "count_sick_ppl", 
	   avg(duree)::interval(0) as "average_timestamp" -- Moyenne de temps arrondie 
from jointab 
where health_status = 'Sick'
group by place_name
-- EXPORT

-- 5. Nombre de sains (non malades) par endroit (place_name) 
-- + durée moyenne de leurs visites dans cet endroit

-- Même script que question précédentes avec filtre sur les personnes non malades
select place_name as "place", count(person_id) as "count_healthy_ppl", 
	   avg(duree)::interval(0) as "average_timestamp_visit"
from jointab 
where health_status != 'Sick'
group by place_name
-- EXPORT

-----------------------------------------------
-----------------------------------------------
-- Exportation par clic/bouton depuis Dbeaver car majorité des tables 
-- créées sont temporaires
-----------------------------------------------
